import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from '../views/HomePage.vue'

Vue.use(VueRouter)
const routes = [
    {
        path: '/',
        name: 'HomePage',
        component: HomePage
    },
    {
      path: '/database',
      name: 'DataBase',
      component: () => import(/* webpackChunkName: "about" */ '../views/DataBase.vue')
    },
    {
      path: '/project',
      name: 'Project',
      component: () => import(/* webpackChunkName: "about" */ '../views/Project.vue')
    },
    {
      path: '/generator',
      name: 'Generator',
      component: () => import(/* webpackChunkName: "about" */ '../views/Generator.vue')
    },
    {
      path: '/template',
      name: 'Template',
      component: () => import(/* webpackChunkName: "about" */ '../views/Template.vue')
    },
    {
      path: '/column-config',
      props: true,
      name: 'TableColumnConfig',
      component: () => import(/* webpackChunkName: "about" */ '../views/TableColumnConfig.vue')
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
