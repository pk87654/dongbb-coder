// 下划线转换驼峰
export function toCamel(str) {
    return str.replace(/\_(\w)/g, function(all, letter){
        return letter.toUpperCase();
    });
}

//下划线转驼峰，并首字母大写
export function firstToUpperCamel(str) {
    return toCamel(str).replace(str[0],str[0].toUpperCase());
}


// 驼峰转换下划线
export function toUnderLine(str) {
    return str.replace(/([A-Z])/g,"_$1").toLowerCase();
}