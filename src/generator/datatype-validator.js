/**
 * 针对每一列设置element-ui数据校验字符串
 * @param column 从数据库里面查出来的列信息
 * @returns string groupName: [{required: true, message: '请输入分组名称', trigger: 'blur'},],
 */
export function elementUIValidStr(column) {
    let required = "";
    if(column.isNullable === 'NO'){
        required = `{required: true, message: '请输入${column.columnComment}', trigger: 'blur'},`
    }
    let maxLength = "";
    if(column.maximumLength !== ''){
        maxLength = `{ max: ${column.maximumLength} , message: '最大长度${column.maximumLength}', trigger: 'change'},`
    }
    return `${column.columnName} :[${required} ${maxLength}],`;
    
}