export let mysqlJavaType =  {
    "bigint":"java.lang.Long",
    "int":"java.lang.Integer",
    "tinyint":"java.lang.Boolean",
    "varchar":"java.lang.String",
    "datetime": "java.util.Date"
};